// Package ctrlevent (已废弃，主要事务已通过消息队列来驱动.) 包含了控制事件的定义。事件一般由管理后台发起。
package ctrlevent

import "acs/pbmodel"

type AppPlatform int32

const (
	PLATFORM_IPHONE AppPlatform = iota
	PLATFORM_ANDROID
	PLATFORM_IPAD
)

type EventType uint16

const (
	EventPatchUpdate EventType = iota
	EventSchemaUpdate
)

type EventRaw struct {
	Type    EventType `json:"type"`
	Content string    `json:"content`
}

type PatchUpdate struct {
	Platform   AppPlatform
	PrevPathId string
	pbmodel.Patch
}

// TODO: schema的更新条件?
type SchemaUpdate struct {
	Platform AppPlatform
	// 前置schemaID
	PrevSchemaId string
	pbmodel.Schema
}

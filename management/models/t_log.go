package models

import (
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/astaxie/beego/orm"
)

type TLog struct {
	Id           int64     `orm:"column(id);auto" description:""  form:"id"  json:"id"`
	LogType      string    `orm:"column(log_type);size(45);null" description:"日志类型"  form:"log_type"  json:"log_type"`
	TUserId      int       `orm:"column(t_user_id)" description:"用户id"  form:"t_user_id"  json:"t_user_id"`
	Content      string    `orm:"column(content);size(105);null" description:"日志内容"  form:"content"  json:"content"`
	Logtime      time.Time `orm:"column(logtime);type(datetime);null" description:"日志产生时间"  form:"logtime"  json:"logtime"`
	UserNickname string    `orm:"column(user_nickname);size(45);null" description:"用户的名称"  form:"user_nickname"  json:"user_nickname"`
}

func (t *TLog) TableName() string {
	return "t_log"
}
func (t *TLog) TableComment() string {
	return ""
}

func init() {
	orm.RegisterModel(new(TLog))
}

// AddTLog insert a new TLog into database and returns
// last inserted Id on success.
func AddTLog(m *TLog) (id int64, err error) {
	o := orm.NewOrm()
	id, err = o.Insert(m)
	return
}

// GetTLogById retrieves TLog by Id. Returns error if
// Id doesn't exist
func GetTLogById(id int64) (v *TLog, err error) {
	o := orm.NewOrm()
	v = &TLog{Id: id}
	if err = o.Read(v); err == nil {
		return v, nil
	}
	return nil, err
}

// GetAllTLog retrieves all TLog matches certain condition. Returns empty list if
// no records exist
func GetAllTLog(query map[string]interface{}, fields []string, sortby []string, order []string,
	offset int64, limit int64, needCount bool) (ml []TLog, totalCount int64, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(TLog))
	// query k=v
	for k, v := range query {
		// rewrite dot-notation to Object__Attribute
		k = strings.Replace(k, ".", "__", -1)
		qs = qs.Filter(k, v)
	}
	// order by:
	var sortFields []string
	if len(sortby) != 0 {
		if len(sortby) == len(order) {
			// 1) for each sort field, there is an associated order
			for i, v := range sortby {
				orderby := ""
				if order[i] == "desc" {
					orderby = "-" + v
				} else if order[i] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
			qs = qs.OrderBy(sortFields...)
		} else if len(sortby) != len(order) && len(order) == 1 {
			// 2) there is exactly one order, all the sorted fields will be sorted by this order
			for _, v := range sortby {
				orderby := ""
				if order[0] == "desc" {
					orderby = "-" + v
				} else if order[0] == "asc" {
					orderby = v
				} else {
					return nil, 0, errors.New("Error: Invalid order. Must be either [asc|desc]")
				}
				sortFields = append(sortFields, orderby)
			}
		} else if len(sortby) != len(order) && len(order) != 1 {
			return nil, 0, errors.New("Error: 'sortby', 'order' sizes mismatch or 'order' size is not 1")
		}
	} else {
		if len(order) != 0 {
			return nil, 0, errors.New("Error: unused 'order' fields")
		}
	}

	var l []TLog
	qs = qs.OrderBy(sortFields...)
	if needCount == true {
		if totalCount, err = qs.Count(); err != nil {

			return nil, 0, err
		}
	}
	if _, err = qs.Limit(limit, offset).All(&l); err == nil {

		return l, totalCount, nil
	}
	return nil, totalCount, err
}

// UpdateTLog updates TLog by Id and returns error if
// the record to be updated doesn't exist
func UpdateTLogById(m *TLog) (vo *TLog, err error) {
	o := orm.NewOrm()
	v := TLog{Id: m.Id}
	// ascertain id exists in the database
	//if err = o.Read(&v); err == nil {
	//var num int64
	if _, err = o.Update(m); err == nil {
		return m, nil
		//	fmt.Println("Number of records updated in database:", num)
	}
	//}
	return &v, nil
}

// DeleteTLog deletes TLog by Id and returns error if
// the record to be deleted doesn't exist
func DeleteTLog(id int64) (err error) {
	o := orm.NewOrm()
	v := TLog{Id: id}
	// ascertain id exists in the database
	if err = o.Read(&v); err == nil {
		var num int64
		if num, err = o.Delete(&TLog{Id: id}); err == nil {
			fmt.Println("Number of records deleted in database:", num)
		}
	}
	return
}

package main

import (
	"acs/pbmodel"
	"acs/taskprocess"
	"fmt"

	"github.com/bitly/go-nsq"
	"gopkg.in/vmihailenco/msgpack.v2"
)

func main() {
	client, err := nsq.NewProducer("172.20.4.200:4150", nsq.NewConfig())
	if err != nil {
		panic(err)
	}
	taskProtoNew := taskprocess.TaskInQueue{
		Type: taskprocess.TaskTypePatch,
	}
	task := &taskprocess.PatchTask{
		PatchTaskToStore: taskprocess.PatchTaskToStore{
			BundleID:    "com.jumei.live.android",
			PrevPatchID: "patch-3.0.2-001",
			Platform:    taskprocess.PlatformAndroid,
			Patch: pbmodel.Patch{
				PatchID: "patch-3.0.2-002",
				Size:    2048576,
				Md5:     "998a7998209f1557a89fbe8c03765bd7",
				Url:     "http://p0.jmstatic.com/moblie/patch/patch-3.0.2-001.patch",
				// patch版本说明
				Desc: `
            <ol>
                <li>修复了xxx</li>
                <li>优化了xxx</li>
            </ol>
            `,
			},
		},
	}

	task.TargetUID = []string{"111", "112", "113", "114", "115", "116", "118"}
	// rb, err := json.Marshal(task)
	rb, err := msgpack.Marshal(task)
	if err != nil {
		panic(err)
	}
	taskProtoNew.RawBytes = rb
	tb, err := msgpack.Marshal(taskProtoNew)
	if err != nil {
		panic(err)
	}

	// tt := &taskprocess.TaskInQueue{}
	// tp := &taskprocess.PatchTask{}
	// msgpack.Unmarshal(tb, tt)
	// fmt.Printf("%+v\n", *tt)
	// msgpack.Unmarshal(tt.RawBytes, tp)
	// fmt.Printf("%+v\n", *tp)
	// return

	err = client.Publish(taskprocess.DefaultTaskTopicName, tb)
	if err != nil {
		panic(err)
	}
	fmt.Printf("published task to queue: %s\n", tb)
	fmt.Printf("published task info: %+v\n", taskProtoNew)
}

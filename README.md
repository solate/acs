# acs
app config server
## 编译
make build
生成 comet/cmd/comet, dispatcher/cmd/dispatcher ,management/management
## 运行
- 运行etcd
- comet/comet -c config.toml  
- dispatcher/dispatcher -c config.toml
- 运行后台管理
- management/management

  默认配置文件在各自目录下。
## comet
app连接管理服务。comet启动后，app可进行连接，comet根据所连接的app信息，及系统中记录的patch/配置schema版本信息给app进行消息推送。

## dispatcher

调度服务: 给app分配可用的comet服务地址.

 - dispatcher地址： 如：http://127.0.0.1:4040/serveraddr  获取服务配置服务地址
 - 返回   
 	{"s": 状态, "m":消息}，例如： 正常返回为{"s": 0, "m":"172.20.80.185:8091"}     
 	异常返回： {"s": 1, "m":"no node to use"}

## etcd 数据存储

#### /acs/ctrl-event
事件通知节点,包括新path信息,一般来自管理后台。

#### /acs/lbs
配置服务器集群节点注册信息(负载均衡信息记录: 记录了每个配置服务器节点的性能相关参数-load、cpu、内存、io 情况).    
e.g. /acs/lbs/00000000000000086075
<pre>
{
server_addr: "127.0.0.1:8082",
info: {
       regTime: 1463998739,
       update_time: 1463998749,
       load:{"One":3.2119140625,
             "Five":3.10791015625,
             "Fifteen":
              3.0703125
            },
       mem: {
               "Total":8589934592, 
               "Used":8560918528, 
               "Free":29016064,
               "ActualFree":2206838784, 
               "ActualUsed":6383095808
            },
       io: 0.10
    }
}
</pre>

    
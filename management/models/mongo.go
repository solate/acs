package models

import (
	"log"

	"gopkg.in/mgo.v2"
)

var UPVUserTaskCollection *mgo.Collection
var UPVTaskContentCollection *mgo.Collection

// ConnectMgo 连接mongo数据库
func InitMgo() error {
	session, err := mgo.Dial(Conf.Mongodb.MongoServerIP)
	if err != nil {
		log.Panic("failed to connect to mongo server:" + err.Error())
		return err
	}
	//	defer session.Close()
	// Optional. Switch the session to a monotonic behavior.
	session.SetMode(mgo.Monotonic, true)
	upvTaskDB := session.DB(Conf.Mongodb.MongoUpvDbname)
	UPVUserTaskCollection = upvTaskDB.C(Conf.Mongodb.MongoUpvUsertaskCollection)
	UPVTaskContentCollection = upvTaskDB.C(Conf.Mongodb.MongoUpvTaskcontentCollection)
	return nil
}

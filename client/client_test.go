package client

import (
	"testing"
	"acs/comet/proto"
	"acs/pbmodel"
	"sync"
)

func TestClient(t *testing.T){
	c, err := NewClient(ClientConfig{
		Addr: "127.0.0.1:8082",
		HeartBeatTtl: 1000,
		Loglevel: LogDebug,
		HandshakeMessage: HandshakeMessage{
			Cmd: proto.CmdRegister,
			ReqMsg: &pbmodel.RegisterInfo{
				Appver: "3.0.1",
				BundleID: "com.jumei.jm.andr oid",
				System: 1,
				OSVersion: "9.3.1",
				Brand: "Huawei",
				Model: "MateS",
				UID: "0",
				UUID: "X-xx",
				LPID: "3.0.1-003",
				LSID: "3.0.1-211",
			},
			Resp:&pbmodel.RegisterInfoResp{},
		},
	})
	if err != nil {
		t.Error(err)
		return
	}
	wg := &sync.WaitGroup{}
	cocur := 100
	errCh := make(chan error, cocur)
	for ;cocur > 0; cocur-- {
		go func(){
			wg.Add(1)
			defer wg.Done()
			for n := 10 ; n > 0 ; n-- {
				_, _, err := c.SendMessage(proto.CmdPathfin, &pbmodel.Patchfin{
					LPID: "3.0.1-004",
				},
					&pbmodel.PatchfinResp{},
				)
				if err != nil {
					errCh <- err
				}
			}
		}()
	}
	wg.Wait()
	select {
	case err = <- errCh:
	default:
	}
	if err != nil {
		t.Error(err)
	}
}

#!/bin/bash
ps ax | grep dispatcher | grep -v sudo |awk '{print $1}' | xargs -I {} sudo kill -9 {}
sleep 5
sudo nohup /home/jm/acs/dispatcher/dispatcher > /home/jm/logs/dispatcher.log &
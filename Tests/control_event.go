package main

import (
	"acs/eventbroker"
	"acs/comet"
	"log"
	"acs/ctrlevent"
	"acs/pbmodel"
	"encoding/json"
)

func main() {
	evbroker, err := eventbroker.NewClient(
		eventbroker.Config{
			Endpoints: comet.Conf.EventBrokerEndpoints,
			Password: "",
			Username: "",
		},
	)
	if err != nil {
		log.Panicf("%v", err)
	}
	patchUpdateEvent := ctrlevent.PatchUpdate{
		Platform: ctrlevent.PLATFORM_ANDROID,
		PrevPathId: "3.0.1-003",
		Patch: pbmodel.Patch{
			PatchID: "3.0.1-004",
			Size: 10240    ,
			Md5:"9fezd0fmcaf9210diooOjf2nnf2221",
			Url:"static.domain.com/patch/android/patch-3.0.1-004",
			Desc: "BUG修复",
		},
	}
	s, _ := json.Marshal(patchUpdateEvent)
	eventData := ctrlevent.EventRaw{
		Type: ctrlevent.EventPatchUpdate,
		Content: string(s),
	}
	s, _ = json.Marshal(eventData)
	err = evbroker.Set(comet.Conf.CtrlEventKey, string(s))
	if err != nil {
		panic(err)
	}
}

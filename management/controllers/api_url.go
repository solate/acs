package controllers

import (
	"acs/management/models"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/labstack/echo"
)

//添加
func AddApiUrlHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	requestbody, requesterr := ioutil.ReadAll(c.Request().Body())
	models.CheckError(requesterr)
	type receiveData struct {
		models.TApiUrl
		Step int64 `json:"step"`
		Page int64 `json:"page"`
	}
	var currentData receiveData
	err = json.Unmarshal(requestbody, &currentData)
	models.CheckError(err)
	currentUser := models.CheckLogin(c)
	if currentData.Id > 0 {
		_, err = models.CheckUserPrivilege(&currentUser, models.TableApiURL, 0, models.RoleUpdate)
		models.CheckError(err)
		_, err = models.UpdateTApiUrlById(&currentData.TApiUrl)
		models.CheckError(err)
		models.AppendLog(&currentUser, "修改api url:"+currentData.Label, models.LOG_TYPE_API_URL)
	} else {
		_, err = models.CheckUserPrivilege(&currentUser, models.TableApiURL, 0, models.RoleCreate)
		models.CheckError(err)
		_, err = models.AddTApiUrl(&currentData.TApiUrl)
		models.CheckError(err)
		models.AppendLog(&currentUser, "添加api url:"+currentData.Label, models.LOG_TYPE_API_URL)
	}
	models.AppendApiUrl2Memery(&currentData.TApiUrl)
	models.CheckError(err)
	datalist, totalNum, listErr := models.GetAllTApiUrl(nil, nil, nil, nil, int64((currentData.Page-1)*currentData.Step), int64(currentData.Step), true)
	models.CheckError(listErr)
	query := make(map[string]interface{})
	query["apiUrllist"] = datalist
	query["apiuserurl"] = models.GetAllApiUserUrlList()
	c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, currentUser, query, totalNum))
	return
}

//获取
func GetApiUrlandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	operationIDStr := c.Param("id")
	operationID, swtichErr := strconv.Atoi(operationIDStr)
	models.CheckError(swtichErr)
	currentUser := models.CheckLogin(c)
	_, err = models.CheckUserPrivilege(&currentUser, models.TableApiURL, operationID, models.RoleRead)
	models.CheckError(err)
	operationItem, dbErr := models.GetTApiUserById(int64(operationID))
	models.CheckError(dbErr)
	c.JSON(http.StatusOK, operationItem)
	return
}

//获取列表
func ListApiUrlHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	currentPageStr := c.Param("page")
	currentPage, switchErr := strconv.Atoi(currentPageStr)
	models.CheckError(switchErr)

	currentStepStr := c.Param("step")
	currentStep, switchErr2 := strconv.Atoi(currentStepStr)
	models.CheckError(switchErr2)
	if currentPage < 1 || currentStep < 1 {
		panic(errors.New("参数不符合标准。"))
	}
	currentUser := models.CheckLogin(c)
	datalist, totalNum, listErr := models.GetAllTApiUrl(nil, nil, nil, nil, int64((currentPage-1)*currentStep), int64(currentStep), true)
	models.CheckError(listErr)
	query := make(map[string]interface{})
	query["apiUrllist"] = datalist
	query["apiuserurl"] = models.GetAllApiUserUrlList()
	c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, currentUser, query, totalNum))
	return
}

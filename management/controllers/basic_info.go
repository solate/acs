package controllers

import (
	"strconv"
	"errors"
	"github.com/labstack/echo"
	"acs/management/models"
	"net/http"
	"strings"
	"io/ioutil"
	"encoding/json"
)


//获取列表
func ListBasicInfoHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)

	//首先进来权限检查
	var currentUser models.UserWithPirvilege
	currentUser = models.CheckLogin(c)
	if currentUser.IsManager != true { //如果不是管理员就不查了
		c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, nil, nil, -1))
		return
	}

	currentPageStr := c.Param("page")
	currentPage, switchErr := strconv.Atoi(currentPageStr)
	models.CheckError(switchErr)
	currentStepStr := c.Param("step")
	currentStep, switchErr2 := strconv.Atoi(currentStepStr)
	models.CheckError(switchErr2)
	if currentPage < 1 || currentStep < 1 {
		panic(errors.New("参数不符合标准。"))
	}

	query := make(map[string]interface{})
	//过滤类型
	basicInfoType := strings.Trim(c.Param("type"), " ")
	if basicInfoType != "*" {
		query["type"] = basicInfoType
	}

	datalist, totalCount, listErr := models.GetAllTBasicInfo(query, nil, nil, nil, int64((currentPage-1)*currentStep), int64(currentStep), true)
	models.CheckError(listErr)

	c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, nil, datalist, totalCount))

	return
}


//添加 或者修改。
func AddBasicInfoHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)

	//首先进来权限检查
	var currentUser models.UserWithPirvilege
	currentUser = models.CheckLogin(c)
	if currentUser.IsManager != true { //如果不是管理员就不查了
		c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, currentUser, nil, -1))
		return
	}

	requestbody, requesterr := ioutil.ReadAll(c.Request().Body())
	models.CheckError(requesterr)
	type receiveData struct {
		models.TBasicInfo
		Step int64 `json:"step"`
		Page int64 `json:"page"`
	}
	//	allStr := c.Param("all")
	var currentData receiveData
	err = json.Unmarshal(requestbody, &currentData)
	models.CheckError(err)
	if currentData.Id > 0 {
		_, err = models.UpdateTBasicInfoById(&currentData.TBasicInfo)
		models.CheckError(err)
		models.AppendLog(&currentUser, "成功修改基础信息:"+currentData.Type+":"+currentData.Value, models.LOG_TYPE_BASIC_INFO)
	} else {
		_, err = models.AddTBasicInfo(&currentData.TBasicInfo)
		models.CheckError(err)
		models.AppendLog(&currentUser, "成功添加基础信息:"+currentData.Type+":"+currentData.Value, models.LOG_TYPE_BASIC_INFO)
	}

	datalist, totalNum, listErr := models.GetAllTBasicInfo(nil, nil, nil, nil, int64((currentData.Page-1)*currentData.Step), int64(currentData.Step), true)
	models.CheckError(listErr)
	c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, currentUser, datalist, totalNum))

	return
}


//删除
func DeleteBasicInfoHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)


	//首先进来权限检查
	var currentUser models.UserWithPirvilege
	currentUser = models.CheckLogin(c)
	if currentUser.IsManager != true { //如果不是管理员就不查了
		c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, currentUser, nil, -1))
		return
	}

	id := strings.Trim(c.Param("id"), " ")
	idInt, err := strconv.ParseInt(id, 10, 64)
	models.CheckError(err)
	_, err = models.DeleteTBasicInfoById(idInt)
	models.CheckError(err)
	c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, nil, nil, -1))
	return nil

}

//获取全部基础信息,在发送消息过滤中使用
func ListBasicInfoByParamsHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)

	system := c.Param("system")
	systemInt, switchErr := strconv.Atoi(system)
	models.CheckError(switchErr)

	query := make(map[string]interface{})
	query["system"] = systemInt

	datalist, _, listErr := models.GetAllTBasicInfo(query, nil, nil, nil, 0, -1, false)
	models.CheckError(listErr)

	c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, nil, datalist, -1))
	return
}
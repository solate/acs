package models

import "github.com/astaxie/beego/orm"

// GetTAppByBundId retrieves TApp by Id. Returns error if
// Id doesn't exist
func GetTAppByBundId(bundid string) (v *TApp, err error) {
	o := orm.NewOrm()
	qs := o.QueryTable(new(TApp))
	v = new(TApp)
	err = qs.Filter("bundle_id", bundid).One(v)
	if err == nil {
		return
	}
	return nil, err
}

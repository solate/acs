package taskprocess

import "acs/pbmodel"

// TaskType to specify the task types.
type TaskType int

const (
	// TaskTypePatch to identify the patch task.
	TaskTypePatch TaskType = iota
	// TaskTypeSchema to identify the schema task.
	TaskTypeSchema
	//TaskTypeEvent 推送的event
	TaskTypeEvent
)

// TaskInQueue is format for storing in queue server.
type TaskInQueue struct {
	Type TaskType
	// RawBytes are to be decoded to the specified task structs.
	RawBytes []byte
}

// PatchTaskToStore patch information that is to be stored.
type PatchTaskToStore struct {
	pbmodel.Patch
	BundleID string
	Platform AppPlatform
	// 前置补丁ID
	PrevPatchID string
}

// PatchTask patch information that is to be sent to the clients.
type PatchTask struct {
	PatchTaskToStore
	TargetUID []string `json:"TargetUID,omitempty"`
}

// SchemaTaskToStore schema information that is to be stored.
type SchemaTaskToStore struct {
	pbmodel.Schema
	BundleID string
	Platform AppPlatform
	// 前置schemaID
	PrevSchemaID string
}

// SchemaTask schema information that is to be sent to the clients.
type SchemaTask struct {
	SchemaTaskToStore
	TargetUID []string `json:"TargetUID,omitempty"`
}

// EventTask event information that is to be sent to the clients.
type EventTask struct {
	pbmodel.Event
	//Content   string
	TargetUID []string `json:"TargetID,omitempty"`


	//过滤条件
	Appver [2]string
	BundleIds []string
	System []int
	OSVersion [2]string
	Brand []string
	Model []string
}

type EventTaskToStore struct {
	pbmodel.Event
	Expiration int64  //过期时间
	AllSend int //是否是*, 1是全部发送，0不是全部发送
}


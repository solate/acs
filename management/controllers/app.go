package controllers

import (
	"acs/management/models"
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/labstack/echo"
)

//添加 或者修改。
func AddAppHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)

	requestbody, requesterr := ioutil.ReadAll(c.Request().Body())
	models.CheckError(requesterr)
	type receiveData struct {
		models.TApp
		Step int64 `json:"step"`
		Page int64 `json:"page"`
	}
	//	allStr := c.Param("all")
	var currentData receiveData
	err = json.Unmarshal(requestbody, &currentData)
	models.CheckError(err)
	currentUser := models.CheckLogin(c)
	currentData.TApp.BundleId = strings.Trim(currentData.TApp.BundleId, " ")
	if currentData.Id > 0 {
		_, err = models.CheckUserPrivilege(&currentUser, models.TableApp, 0, models.RoleUpdate)
		models.CheckError(err)
		_, err = models.UpdateTAppById(&currentData.TApp)
		models.CheckError(err)
		models.AppendLog(&currentUser, "成功修改app:"+currentData.BundleId, models.LOG_TYPE_APP)
	} else {
		_, err = models.CheckUserPrivilege(&currentUser, models.TableApp, 0, models.RoleCreate)
		models.CheckError(err)
		_, err = models.AddTApp(&currentData.TApp)
		models.CheckError(err)
		models.AppendLog(&currentUser, "成功添加app:"+currentData.BundleId, models.LOG_TYPE_APP)
		//刚添加的app  那么添加人对它拥有完全的权限。将权限写入数据库
		privilitelist, listerr := models.InsertAllPrivilege(currentUser.Username, "t_app", int(currentData.Id))
		models.CheckError(listerr)
		models.AddPrivilegeToUserPrivilegeList(&currentUser, privilitelist)
	}
	c.JSON(http.StatusOK, currentData)
	return
}

//获取
func GetAppandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	operationIDStr := c.Param("id")
	operationID, swtichErr := strconv.Atoi(operationIDStr)
	models.CheckError(swtichErr)
	currentUser := models.CheckLogin(c)
	_, err = models.CheckUserPrivilege(&currentUser, models.TableApp, operationID, models.RoleRead)
	models.CheckError(err)
	operationItem, dbErr := models.GetTAppById(int64(operationID))
	models.CheckError(dbErr)
	c.JSON(http.StatusOK, operationItem)
	return
}

//获取列表
func ListAppHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	currentPageStr := c.Param("page")
	currentPage, switchErr := strconv.Atoi(currentPageStr)
	models.CheckError(switchErr)
	currentStepStr := c.Param("step")
	currentStep, switchErr2 := strconv.Atoi(currentStepStr)
	models.CheckError(switchErr2)
	if currentPage < 1 || currentStep < 1 {
		panic(errors.New("参数不符合标准。"))
	}
	allStr := c.Param("all")
	query := make(map[string]interface{})
	var currentUser models.UserWithPirvilege
	currentUser = models.CheckLogin(c)
	allowList, _ := models.CheckUserPrivilege(&currentUser, models.TableApp, 0, models.RoleRead)
	if len(allowList) == 0 {
		c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, currentUser, []int{}, -1))
		return
	} else if len(allowList) == 1 && allowList[0] == 0 {
		// 可以拿到全部的数据
	} else {
		query["id__in"] = allowList
	}
	if allStr == "0" {
		// 不全拿，只拿可用的
		query["enable"] = 1
	}


	datalist, _, listErr := models.GetAllTApp(query, nil, nil, nil, int64((currentPage-1)*currentStep), int64(currentStep), false)
	models.CheckError(listErr)


	c.JSON(http.StatusOK, models.ExportJson(models.Right_Status, currentUser, datalist, -1))

	return
}

package main

import "fmt"

func main() {
	a := []int{2, 1, 2, 5, 6, 3, 4, 5, 2, 3, 9}
	z := UniqueSlice(&a)
	fmt.Println(z)


}


func UniqueSlice(slice *[]int) []int {
	found := make(map[int]bool)
	result := []int{}
	for _, val := range *slice {
		if _, ok := found[val]; !ok {
			found[val] = true
			result = append(result, val)

		}
	}

	return result
}
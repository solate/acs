package client

type LogLevel uint8
const (
	LogPanic LogLevel = iota
	LogFatal
	LogError
	LogWarn
	LogInfo
	LogDebug
)

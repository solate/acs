package controllers

import (
	"acs/management/models"
	"net/http"

	"github.com/chaosue/echo-sessions"
	"github.com/labstack/echo"
)

//UserIndexHandler 主页
func UserIndexHandler(c echo.Context) (err error) {
	defer models.ErrorHandler(c)
	session := sessions.Default(c)
	manager := session.Get(models.SESSION_USER)
	if manager == nil {
		if models.Conf.AppInfo.Runmode == "dev" {
			err = c.Redirect(http.StatusTemporaryRedirect, models.VIEW_INDEX_URL)
		} else {
			err = c.Redirect(http.StatusTemporaryRedirect, models.Conf.JumeiAuth.JumeiAuthUrl+models.Conf.JumeiAuth.JumeiAuthApiLogin+"/?"+models.Conf.JumeiAuth.JumeIAuthLoginUrl+"&"+models.Conf.JumeiAuth.JumeIAuthAcs)
		}
		models.CheckError(err)
	} else {
		//有session的直接开主页。
		err = c.Redirect(http.StatusTemporaryRedirect, models.VIEW_INDEX_URL)
		models.CheckError(err)
	}
	return
}

package eventbroker

const DefaultBrokerAddr = "127.0.0.1:2379"

type WatchResponse struct {
	Key string
	Value string
	Action string
}